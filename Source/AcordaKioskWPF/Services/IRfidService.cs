﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AcordaKioskWPF.Model;

namespace AcordaKioskWPF.Services
{
    public interface IRfidService
    {
        Task<IEnumerable<Visitor>> GetCurrentVisitors(string rfidReaderIP, int rfidReaderAntenna);
    }
}
