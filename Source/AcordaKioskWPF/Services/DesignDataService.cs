﻿using System;
using System.Linq;
using AcordaKioskWPF.Model;

namespace AcordaKioskWPF.Services
{
    public class DesignDataService : BaseDataService
    {
        public override void UserLogin(string badgeId, string stationName)
        {
            base.UserLogin(badgeId, stationName);
            Logger.Warn("Fake user login!");
        }

        public override void WriteUserAnswer(string badgeId, int questionId, string questionText, int answerId, string answerText)
        {
            Logger.Warn("Fake user answer!");
        }

        public override KioskSurveyQuestion GetUserAnswer(string badgeId, int questionId)
        {
            Logger.Warn("Getting fake (random hcp type) useranswer!");
            var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.HcpType);
            var r = new Random();
            var idx = r.Next(0, answer.AnswerMapping.Count);
            answer.SelectedAnswers.Add(KioskSurveyQuestion.HcpType.AnswerMapping.Skip(idx).First().Key);
            return answer;
        }

        public override void ClearUserAnswers(string badgeId, int questionId)
        {
            Logger.Warn("Fake clearing of useranswers!");
        }

        public override bool HasSurvey(string badgeId)
        {
            Logger.Warn("Fake survey check!");
            return false;
        }

        public override void WritePageview(string badgeId, string stationName, string section, int pageNum)
        {
            Logger.Warn("Fake pageview!");
        }
    }
}
