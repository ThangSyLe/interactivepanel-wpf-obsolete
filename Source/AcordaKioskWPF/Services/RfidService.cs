﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AcordaKioskWPF.Model;
using NLog;

namespace AcordaKioskWPF.Services
{
    public class RfidService : IRfidService
    {
        private readonly Logger _logger;
        public RfidService() { _logger = LogManager.GetCurrentClassLogger(); }

        public async Task<IEnumerable<Visitor>> GetCurrentVisitors(string rfidReaderIP, int rfidReaderAntenna)
        {
            _logger.Debug($"Getting rfid visitors for reader mac[{ rfidReaderIP }] and antenna[{ rfidReaderAntenna }]");
            using (var ctx = new AcordaModel())
            {
                var visitor_join = from v in ctx.Visitors
                    join t in ctx.RFIDReadTags on v.RFIDTagNum equals t.RFIDTagNum
                    where t.ReaderIPAddr.Equals(rfidReaderIP) && t.Antenna == rfidReaderAntenna && t.RSSI > 0
                    select v;

                return await visitor_join.ToListAsync().ContinueWith((t) =>
                {
                    if (!t.IsFaulted) return t.Result.AsEnumerable();

                    var error_txt = "Error getting rfid visitors from db!";
                    var exceptions = t.Exception?.Flatten().InnerExceptions;
                    if (exceptions != null)
                        error_txt = exceptions.Aggregate(error_txt, (current, ex) => current + $"\r\n ex[{ex.Message}]");
                    _logger.Error(t.Exception, error_txt);
                    return new List<Visitor>();
                });
            }
        }
    }
}
