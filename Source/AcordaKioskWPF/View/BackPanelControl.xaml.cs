﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using AcordaKioskWPF.ViewModel;
using GalaSoft.MvvmLight;

namespace AcordaKioskWPF.View
{
    /// <summary>
    /// Interaction logic for BackPanelControl.xaml
    /// </summary>
    public partial class BackPanelControl : UserControl
    {
        public ViewModelBase DisplayedContent
        {
            get { return (ViewModelBase)GetValue(DisplayedContentProperty); }
            set { SetValue(DisplayedContentProperty, value); }
        }
        public static readonly DependencyProperty DisplayedContentProperty = 
            DependencyProperty.Register(
                "DisplayedContent", 
                typeof(ViewModelBase), 
                typeof(BackPanelControl), 
                new PropertyMetadata(ContentChanged));

        private static void ContentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue == null) return;

            var model_name = args.NewValue.GetType().ToString();
            var ctrl = (BackPanelControl) obj;
            ctrl.AnimateBackPanel(model_name);
        }

        public void AnimateBackPanel(string modelName)
        {
            if (modelName == typeof(NameSelectViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 353, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }else if (modelName == typeof(NameConfirmViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 353, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
            else if (modelName == typeof(SelectTypeViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 70, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
            else if (modelName == typeof(SelectSpecialtyViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 70, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
            else if (modelName == typeof(SelectOffPeriodsViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 301, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
            else if (modelName == typeof(SelectSymptomsViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 0, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
            else if (modelName == typeof(SelectFeelingsViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 0, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
            else if (modelName == typeof(SelectWordViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 70, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
            else if (modelName == typeof(SelectLimitViewModel).ToString())
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 0, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
            else
            {
                var anim = new DoubleAnimation(_panelTransform.Y, 1066, TimeSpan.FromMilliseconds(250));
                _panelTransform.BeginAnimation(TranslateTransform.YProperty, anim);
            }
        }

        private readonly TranslateTransform _panelTransform;

        public BackPanelControl()
        {
            InitializeComponent();
            _panelTransform = new TranslateTransform();
            PanelImage.RenderTransform = _panelTransform;
        }
    }
}
