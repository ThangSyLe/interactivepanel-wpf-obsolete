﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using NLog;

namespace AcordaKioskWPF.View
{
    /// <summary>
    /// Interaction logic for VideoPlayer.xaml
    /// </summary>
    public partial class VideoPlayer : UserControl
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly TranslateTransform _scrubberTransform = new TranslateTransform();

        public VideoPlayer()
        {
            InitializeComponent();
            ScrubberHandle.RenderTransform = _scrubberTransform;
        }

        private void MediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            _logger.Error(e.ErrorException, $"Patient video failed to play. ex[{ e.ErrorException.Message }]");
        }

        private void MediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            _logger.Debug("Patient video media opened");
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            _logger.Debug("Patient video ended");
        }

        private DispatcherTimer _progressTimer;

        private void VideoPlayer_OnLoaded(object sender, RoutedEventArgs e)
        {
            PatientVideo.IsMuted = true;
            PatientVideo.Play();
            Task.Delay(500).ContinueWith(t =>
            {
                Dispatcher.Invoke(() =>
                {
                    PatientVideo.Pause();
                    PatientVideo.IsMuted = false;
                    PatientVideo.Position = TimeSpan.Zero;

                    var anim = new DoubleAnimation(1, 0, TimeSpan.FromMilliseconds(500));
                    anim.Completed += (obj, argus) =>
                    {
                        PatientVideo.Play();
                        _progressTimer = new DispatcherTimer
                        {
                            Interval = TimeSpan.FromMilliseconds(50)
                        };
                        _progressTimer.Tick += (o, args) =>
                        {
                            if (!PatientVideo.NaturalDuration.HasTimeSpan) return;
                            var time_ratio = PatientVideo.Position.TotalMilliseconds / PatientVideo.NaturalDuration.TimeSpan.TotalMilliseconds;
                            _scrubberTransform.X = SCRUB_BAR_SIZE * time_ratio;  // position where the handle sits at the end of the scrub bar
                        };
                        _progressTimer.IsEnabled = true;
                    };
                    ContentGrid.BeginAnimation(OpacityProperty, anim);
                });
            });
        }

        public const int PLAY_CIRCLE_SIZE = 42;
        public const int SCRUB_BAR_SIZE = 961;

        private void Image_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            var scrub_to = e.GetPosition((Image)sender).X - PLAY_CIRCLE_SIZE; // size of play circle on the left
            var ratio = scrub_to / SCRUB_BAR_SIZE;
            PatientVideo.Position = TimeSpan.FromMilliseconds( PatientVideo.NaturalDuration.TimeSpan.TotalMilliseconds * ratio );
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            PatientVideo.Stop();
            _progressTimer.Stop();
        }
    }
}
