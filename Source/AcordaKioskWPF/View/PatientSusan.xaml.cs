﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AcordaKioskWPF.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.View
{
    /// <summary>
    /// Interaction logic for PatientSusan.xaml
    /// </summary>
    public partial class PatientSusan : UserControl
    {
        private bool _vidPlaying;

        public PatientSusan()
        {
            InitializeComponent();

            Messenger.Default.Register<NextPageMessage>(this, msg =>
            {
                if (!_vidPlaying) return;

                Messenger.Default.Send(new VideoStateChangedMessage { IsPlaying = _vidPlaying, VideoName = "Susan" });
                SusanVideo.Stop();
                _vidPlaying = false;
            });

            Messenger.Default.Register<PushFooterMessage>(this, msg =>
            {
                if (!_vidPlaying) return;
                SusanVideo.Pause();
                _vidPlaying = false;
            });
        }

        private void SusanVideo_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            PosterImage.Visibility = Visibility.Hidden;
            if(!_vidPlaying)
                SusanVideo.Play();
            else
                SusanVideo.Pause();
            _vidPlaying = !_vidPlaying;

            Messenger.Default.Send(new VideoStateChangedMessage { IsPlaying = _vidPlaying, VideoName = "Susan" });
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // preload the video under a poster image so that we don't get any dumb stuttering
            SusanVideo.IsMuted = true;
            SusanVideo.Play();
            Task.Delay(1000).ContinueWith(t =>
            {
                Dispatcher.Invoke(() =>
                {
                    SusanVideo.Pause();
                    SusanVideo.IsMuted = false;
                    SusanVideo.Position = TimeSpan.Zero;
                    PosterImage.IsEnabled = true;
                });
            });
        }

        private void SusanVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(new VideoStateChangedMessage { IsPlaying = _vidPlaying, VideoName = "Susan" });
            Task.Delay(2000).ContinueWith(t =>
            {
                Dispatcher.Invoke(() =>
                {
                    PosterImage.Visibility = Visibility.Visible;
                    SusanVideo.Position = TimeSpan.Zero;
                    SusanVideo.Pause();
                    _vidPlaying = false;
                });
            });
        }
    }
}
