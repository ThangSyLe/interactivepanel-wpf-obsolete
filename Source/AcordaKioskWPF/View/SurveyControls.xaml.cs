﻿using System;
using System.Windows;
using System.Windows.Controls;
using AcordaKioskWPF.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.View
{
    /// <summary>
    /// Interaction logic for SurveyControls.xaml
    /// </summary>
    public partial class SurveyControls : UserControl
    {
        public const int NUM_SURVEY_PAGES = 7;

        public int PageNumber
        {
            get { return (int)GetValue(PageNumberProperty); }
            set { SetValue(PageNumberProperty, value); }
        }
        public static readonly DependencyProperty PageNumberProperty =
            DependencyProperty.Register(
                "PageNumber", 
                typeof(int), 
                typeof(SurveyControls), 
                new PropertyMetadata(1, PageNumberChanged));

        private static void PageNumberChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.NewValue == null) return;
            var ctrl = (SurveyControls) obj;
            var pgnum = (int) args.NewValue;
            if (pgnum <= 1)
                ctrl.PreviousVisible = false;
            if (pgnum > 1)
                ctrl.PreviousVisible = true;
            //if (pgnum >= 7)
            //    ctrl.NextVisible = false;
            //if (pgnum < 7)
            //    ctrl.NextVisible = true;

            var width = (pgnum - 1)*49 + 19;
            ctrl.CheckMaskWidth = width;
        }

        public bool NextVisible
        {
            get { return (bool)GetValue(NextVisibleProperty); }
            set { SetValue(NextVisibleProperty, value); }
        }
        public static readonly DependencyProperty NextVisibleProperty = DependencyProperty.Register("NextVisible", typeof(bool), typeof(SurveyControls), new PropertyMetadata(true));

        public bool PreviousVisible
        {
            get { return (bool)GetValue(PreviousVisibleProperty); }
            set { SetValue(PreviousVisibleProperty, value); }
        }
        public static readonly DependencyProperty PreviousVisibleProperty =
            DependencyProperty.Register("PreviousVisible", typeof(bool), typeof(SurveyControls), new PropertyMetadata(false));

        public double CheckMaskHeight
        {
            get { return (double)GetValue(CheckMaskHeightProperty); }
            set { SetValue(CheckMaskHeightProperty, value); }
        }
        public static readonly DependencyProperty CheckMaskHeightProperty =
            DependencyProperty.Register("CheckMaskHeight", typeof(double), typeof(SurveyControls), new PropertyMetadata(100.0));

        public double CheckMaskWidth
        {
            get { return (double)GetValue(CheckMaskWidthProperty); }
            set { SetValue(CheckMaskWidthProperty, value); }
        }
        public static readonly DependencyProperty CheckMaskWidthProperty =
            DependencyProperty.Register("CheckMaskWidth", typeof(double), typeof(SurveyControls), new PropertyMetadata(19.0));

        public bool OkVisible
        {
            get { return (bool)GetValue(OkVisibleProperty); }
            set { SetValue(OkVisibleProperty, value); }
        }
        public static readonly DependencyProperty OkVisibleProperty =
            DependencyProperty.Register("OkVisible", typeof(bool), typeof(SurveyControls), new PropertyMetadata(false));

        public SurveyControls()
        {
            InitializeComponent();
        }

        private void OkButton_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(new AnswerAcceptedMessage());
            if(PageNumber >= NUM_SURVEY_PAGES)
                Messenger.Default.Send(new SurveyFinishedMessage());
        }

        private void NextButton_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(new SurveyQuestionSelectedMessage());    // send a blank question response to clear out before moving forward
            Messenger.Default.Send(new AnswerAcceptedMessage());
            if (PageNumber >= NUM_SURVEY_PAGES)
                Messenger.Default.Send(new SurveyFinishedMessage());
        }

        private void PrevButton_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(new PreviousQuestionMessage());
        }
    }
}
