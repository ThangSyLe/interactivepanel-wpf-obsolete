﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;

namespace AcordaKioskWPF.View
{
    /// <summary>
    /// Interaction logic for AttractLoop.xaml
    /// </summary>
    public partial class AttractLoop : UserControl
    {

        private readonly Logger _logger;
        public AttractLoop()
        {
            InitializeComponent();
            _logger = LogManager.GetCurrentClassLogger();
        }

        private void MediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            _logger.Error(e.ErrorException, $"Attract loop failed to play. ex[{ e.ErrorException.Message }]");
        }

        private void MediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            _logger.Debug("Attract loop media opened");
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            _logger.Debug("Attract loop ended");
        }
    }
}
