﻿using System.Windows;
using AcordaKioskWPF.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using NLog;

namespace AcordaKioskWPF
{
    public partial class MainWindow
    {
        private readonly Logger _logger;
        public MainWindow()
        {
            InitializeComponent();
            _logger = LogManager.GetCurrentClassLogger();
        }

        private void MediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            _logger.Error(e.ErrorException, $"Attract loop failed to play. ex[{ e.ErrorException.Message }]");
        }

        private void MediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            _logger.Debug("Attract loop media opened");
        }

        private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            _logger.Debug("Attract loop ended");
        }
    }
}
