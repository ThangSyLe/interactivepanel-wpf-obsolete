﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class SelectSymptomsViewModel : ViewModelBase
    {
        private bool _isLoading;

        private RelayCommand _loadedCommand;
        public RelayCommand LoadedCommand => _loadedCommand ?? (_loadedCommand = new RelayCommand(() =>
        {
            SetQuestionTextFromType();

            _isLoading = true;

            var selected = _symptoms.FirstOrDefault(n => n.IsSelected);
            if (selected != null) selected.IsSelected = false;

            _isLoading = false;

            // reset any selected values from this session
            var answer = _dataService.GetCachedAnswer(KioskSurveyQuestion.Symptom.QuestionId);
            if (answer == null || answer.SelectedAnswers.Count == 0) return;
            foreach (var selected_answer in answer.SelectedAnswers)
            {
                var spec = _symptoms.FirstOrDefault(s => s.Value == selected_answer);
                if (spec != null) spec.IsSelected = true;
            }
        }));

        private string _questionText;
        public string QuestionText
        {
            get { return _questionText; }
            set
            {
                if (_questionText == value) return;
                _questionText = value;
                RaisePropertyChanged();
            }
        }

        public const int DONT_HAVE_AID = 12;
        public const int DONT_KNOW_AID = 13;

        private ObservableCollection<SelectedObject> _symptoms;
        public ObservableCollection<SelectedObject> Symptoms
        {
            get
            {
                if (_symptoms != null) return _symptoms;
                _symptoms = GetSymptomsFromHcpType();
                return _symptoms;
            }
            set
            {
                _symptoms = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<SelectedObject> GetSymptomsFromHcpType(int type = -1)
        {
            var mapping = KioskSurveyQuestion.Symptom.AnswerMapping;
            switch (type) {
                case UserType.HCP:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_KNOW_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                        );
                case UserType.OTHER:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_HAVE_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                        );
                default:
                    return new ObservableCollection<SelectedObject>(
                        mapping
                            .Where(m => m.Value != DONT_HAVE_AID && m.Value != DONT_KNOW_AID)
                            .Select(k => new SelectedObject(k.Key, SpecialtyPropertyChanged)).ToArray()
                        );
            }
        }

        // ensure only one name can be selected at once
        private void SpecialtyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected" || _isLoading) return;

            Messenger.Default.Send(new SurveyQuestionSelectedMessage());

            var answer = KioskSurveyQuestion.QuestionFromTemplate(KioskSurveyQuestion.Symptom);
            answer.SelectedAnswers = _symptoms.Where(o => o.IsSelected).Select(o => o.Value).ToList();
            _dataService.CacheSurveyAnswer(answer);
        }

        public void SetQuestionTextFromType()
        {
            var text = "";
            var type = UserType.IdFromType(_dataService.GetUserType());
            switch (type)
            {
                case UserType.PWP:
                    text += "What are some of the physical symptoms you experience during an OFF period?";
                    break;
                case UserType.CAREGIVER:
                    text += "What are some of the physical symptoms you observe in your loved one during an OFF period?";
                    break;
                case UserType.HCP:
                    text += "What are some of the physical symptoms your PD patients report experiencing during OFF periods?";
                    break;
                case UserType.OTHER:
                    text += "What are some of the physical symptoms you associate with OFF periods?";
                    break;
            }
            QuestionText = text;
            Symptoms = GetSymptomsFromHcpType(type);
        }

        private readonly IDataService _dataService;

        public SelectSymptomsViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }
    }
}
