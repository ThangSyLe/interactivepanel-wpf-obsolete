﻿using AcordaKioskWPF.Messages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class VideoPlayerViewModel : ViewModelBase
    {
        private string _videoPath;
        public string VideoPath
        {
            get { return _videoPath; }
            set
            {
                if (_videoPath == value) return;
                _videoPath = value;
                RaisePropertyChanged();
            }
        }

        private RelayCommand _moreStoriesCommand;
        public RelayCommand MoreStoriesCommand
        {
            get
            {
                return _moreStoriesCommand ?? (_moreStoriesCommand = new RelayCommand(() =>
                {
                    Messenger.Default.Send(new NextPageMessage(typeof(VideoPlayerViewModel)));
                }));
            }
        }

        public override void Cleanup()
        {
            base.Cleanup();
            // a little clunky, but the one in the constructor seems to get killed after the first run
            Messenger.Default.Register<PlayVideoMessage>(this, msg =>
            {
                VideoPath = msg.VideoPath;
            });
        }

        public VideoPlayerViewModel()
        {
            Messenger.Default.Register<PlayVideoMessage>(this, msg =>
            {
                VideoPath = msg.VideoPath;
            });
        }
    }
}
