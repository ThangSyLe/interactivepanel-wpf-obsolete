using System;
using AcordaKioskWPF.Properties;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace AcordaKioskWPF.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic || !Settings.Default.UseRfid)
            {
                SimpleIoc.Default.Register<IRfidService, DesignRfidService>();
            }
            else
            {
                SimpleIoc.Default.Register<IRfidService, RfidService>();
            }

            if (ViewModelBase.IsInDesignModeStatic || !Settings.Default.UseDatabase)
            {
                SimpleIoc.Default.Register<IDataService, DesignDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<IDataService, DataService>();
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<AttractLoopViewModel>();
            SimpleIoc.Default.Register<NameSelectViewModel>();
            SimpleIoc.Default.Register<NameConfirmViewModel>();
            SimpleIoc.Default.Register<SelectTypeViewModel>();
            SimpleIoc.Default.Register<SelectSpecialtyViewModel>();
            SimpleIoc.Default.Register<SelectOffPeriodsViewModel>();
            SimpleIoc.Default.Register<SelectSymptomsViewModel>();
            SimpleIoc.Default.Register<SelectFeelingsViewModel>();
            SimpleIoc.Default.Register<SelectWordViewModel>();
            SimpleIoc.Default.Register<SelectLimitViewModel>();
            SimpleIoc.Default.Register<SurveyViewModel>();
            SimpleIoc.Default.Register<VideoSelectorViewModel>();
            SimpleIoc.Default.Register<VideoPlayerViewModel>();
            SimpleIoc.Default.Register<ContentPageViewModel>();
            SimpleIoc.Default.Register<FooterPageViewModel>();
            SimpleIoc.Default.Register<PIViewModel>();
        }

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public AttractLoopViewModel AttractLoop => ServiceLocator.Current.GetInstance<AttractLoopViewModel>();
        public NameSelectViewModel NameSelect => ServiceLocator.Current.GetInstance<NameSelectViewModel>();
        public NameConfirmViewModel NameConfirm => ServiceLocator.Current.GetInstance<NameConfirmViewModel>();
        public SelectTypeViewModel SelectType => ServiceLocator.Current.GetInstance<SelectTypeViewModel>();
        public SelectSpecialtyViewModel SelectSpecialty => ServiceLocator.Current.GetInstance<SelectSpecialtyViewModel>();
        public SelectOffPeriodsViewModel SelectOff => ServiceLocator.Current.GetInstance<SelectOffPeriodsViewModel>();
        public SelectSymptomsViewModel SelectSymptoms => ServiceLocator.Current.GetInstance<SelectSymptomsViewModel>();
        public SelectFeelingsViewModel SelectFeelings => ServiceLocator.Current.GetInstance<SelectFeelingsViewModel>();
        public SelectWordViewModel SelectWord => ServiceLocator.Current.GetInstance<SelectWordViewModel>();
        public SelectLimitViewModel SelectLimit => ServiceLocator.Current.GetInstance<SelectLimitViewModel>();
        public SurveyViewModel Survey => ServiceLocator.Current.GetInstance<SurveyViewModel>();
        public VideoSelectorViewModel VideoSelector => ServiceLocator.Current.GetInstance<VideoSelectorViewModel>(Guid.NewGuid().ToString());
        public VideoPlayerViewModel VideoPlayer => ServiceLocator.Current.GetInstance<VideoPlayerViewModel>();
        public ContentPageViewModel ContentPage => ServiceLocator.Current.GetInstance<ContentPageViewModel>();
        public FooterPageViewModel FooterPage => ServiceLocator.Current.GetInstance<FooterPageViewModel>();
        public PIViewModel PI => ServiceLocator.Current.GetInstance<PIViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}