﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class VideoSelectorViewModel : ViewModelBase
    {
        private ObservableCollection<VideoSelection> _videoSelections;
        public ObservableCollection<VideoSelection> VideoSelections
        {
            get { return _videoSelections; }
            set
            {
                if (_videoSelections == value) return;
                _videoSelections = value;
                RaisePropertyChanged();
            }
        }

        private VideoSelection _selection;
        public VideoSelection Selection
        {
            get { return _selection; }
            set
            {
                if (_selection == value) return;
                _selection = value;
                RaisePropertyChanged();
            }
        }

        private RelayCommand _nextVideoCommand;
        public RelayCommand NextVideoCommand
        {
            get
            {
                return _nextVideoCommand ?? (_nextVideoCommand = new RelayCommand(() =>
                {
                    var current_idx = VideoSelections.IndexOf(Selection);
                    if (current_idx >= VideoSelections.Count - 1) return;

                    UpdateVideoIndex(current_idx + 1);
                }));
            }
        }

        private RelayCommand _previousVideoCommand;
        public RelayCommand PreviousVideoCommand
        {
            get
            {
                return _previousVideoCommand ?? (_previousVideoCommand = new RelayCommand(() =>
                {
                    var current_idx = VideoSelections.IndexOf(Selection);
                    if (current_idx == 0) return;

                    UpdateVideoIndex(current_idx - 1);
                }));
            }
        }

        public void UpdateVideoIndex(int idx)
        {
            Selection = VideoSelections[idx];
            StoryLabel = $"Play {Selection.PatientName}'s Story.";

            if (idx >= VideoSelections.Count - 1)
            {
                NextButtonEnabled = false;
            }
            else
            {
                NextButtonEnabled = true;
                NextButtonLabel = VideoSelections[idx + 1].PatientName;
            }

            if (idx == 0)
            {
                PrevButtonEnabled = false;
            }
            else
            {
                PrevButtonEnabled = true;
                PrevButtonLabel = VideoSelections[idx - 1].PatientName;
            }
        }

        private string _nextBtnLabel;
        public string NextButtonLabel
        {
            get { return _nextBtnLabel; }
            set
            {
                if (_nextBtnLabel == value) return;
                _nextBtnLabel = value;
                RaisePropertyChanged();
            }
        }

        private string _prevBtnLabel;
        public string PrevButtonLabel
        {
            get { return _prevBtnLabel; }
            set
            {
                if (_prevBtnLabel == value) return;
                _prevBtnLabel = value;
                RaisePropertyChanged();
            }
        }

        private bool _nextBtnEnabled;
        public bool NextButtonEnabled
        {
            get { return _nextBtnEnabled; }
            set
            {
                if (_nextBtnEnabled == value) return;
                _nextBtnEnabled = value;
                RaisePropertyChanged();
            }
        }

        private bool _prevBtnEnabled;
        public bool PrevButtonEnabled
        {
            get { return _prevBtnEnabled; }
            set
            {
                if (_prevBtnEnabled == value) return;
                _prevBtnEnabled = value;
                RaisePropertyChanged();
            }
        }

        private string _storyLabel;
        public string StoryLabel
        {
            get { return _storyLabel; }
            set
            {
                if (_storyLabel == value) return;
                _storyLabel = value;
                RaisePropertyChanged();
            }
        }

        private RelayCommand _playVideoCommand;
        public RelayCommand PlayVideoCommand
        {
            get
            {
                return _playVideoCommand ?? (_playVideoCommand = new RelayCommand(() =>
                {
                    var selected_path = Selection.VideoPath;
                    _dataService.MarkPatientWatched(Selection.PatientName);
                    Messenger.Default.Send(new NextPageMessage(typeof(VideoSelectorViewModel)));    // this will cause cleanup, which will reset the selection to default
                    Messenger.Default.Send(new PlayVideoMessage { VideoPath = selected_path });
                }));
            }
        }

        public override void Cleanup()
        {
            base.Cleanup();
            MarkPatientsWatched();
            UpdateVideoIndex(2);
        }

        private readonly IDataService _dataService;

        public VideoSelectorViewModel(IDataService dataService)
        {
            _dataService = dataService;
            VideoSelections = new ObservableCollection<VideoSelection>(VideoSelection.DefaultSelections);
            var patients = MarkPatientsWatched();
            //if(!patients.Any())
                UpdateVideoIndex(2);
        }

        private string[] MarkPatientsWatched()
        {
            var patients = _dataService.GetWatchedPatients();
            foreach (var v in VideoSelections) {
                v.VideoWatched = patients.Contains(v.PatientName);
            }
            return patients;
        }
    }

    public class VideoSelection
    {
        public string Patient { get; set; }
        public string PatientName { get; set; }
        public string PullQuote { get; set; }
        public string BgUrl { get; set; }
        public string VideoPath { get; set; }
        public bool VideoWatched { get; set; }

        public static VideoSelection[] DefaultSelections = {
            new VideoSelection
            {
                Patient = Pages.PatientMikeB,
                PatientName = "Mike B.",
                PullQuote = "“I feel like Mike's whole body is in chaos.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/bgMikeB.png",
                VideoPath = "pack://siteoforigin:,,,/Resources/MikeB.mp4",
            },
            new VideoSelection
            {
                Patient = Pages.PatientSteve,
                PatientName = "Steve",
                PullQuote = "“I feel like Steve's whole body is in chaos.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/bgSteve.png",
                VideoPath = "pack://siteoforigin:,,,/Resources/Steve.mp4"
            },
            new VideoSelection
            {
                Patient = Pages.PatientBrenda,
                PatientName = "Brenda",
                PullQuote = "“I feel like my whole body is in chaos.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/bgBrenda.png",
                VideoPath = "pack://siteoforigin:,,,/Resources/Brenda.mp4"
            },
            new VideoSelection
            {
                Patient = Pages.PatientMikeA,
                PatientName = "Mike A.",
                PullQuote = "“I feel like Mikey's whole body is in chaos.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/bgMikeA.png",
                VideoPath = "pack://siteoforigin:,,,/Resources/MikeA.mp4"
            },
            new VideoSelection
            {
                Patient = Pages.PatientIsrael,
                PatientName = "Israel",
                PullQuote = "“I feel like Israel's whole body is in chaos.”",
                BgUrl = "pack://siteoforigin:,,,/Resources/bgIsrael.png",
                VideoPath = "pack://siteoforigin:,,,/Resources/Israel.mp4"
            }
        };
    }
}
