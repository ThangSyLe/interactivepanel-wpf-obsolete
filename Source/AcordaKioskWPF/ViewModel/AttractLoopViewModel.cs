﻿using AcordaKioskWPF.Messages;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class AttractLoopViewModel : ViewModelBase
    {
        // handle a manual tap on the screen
        private RelayCommand _nextPageCommand;
        public RelayCommand NextPageCommand
        {
            get
            {
                return _nextPageCommand ?? (_nextPageCommand = new RelayCommand(() =>
                {
                    Messenger.Default.Send(new NextPageMessage(GetType()));
                }));
            }
        }
    }
}