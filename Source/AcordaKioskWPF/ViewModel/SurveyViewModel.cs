﻿using AcordaKioskWPF.Messages;
using AcordaKioskWPF.Model;
using AcordaKioskWPF.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace AcordaKioskWPF.ViewModel
{
    public class SurveyViewModel : ViewModelBase
    {
        private RelayCommand _nextPageCommand;
        public RelayCommand NextPageCommand
        {
            get
            {
                return _nextPageCommand ?? (_nextPageCommand = new RelayCommand(() =>
                {
                    var default_video = VideoSelection.DefaultSelections[2];
                    _dataService.MarkPatientWatched(default_video.PatientName);
                    Messenger.Default.Send(new NextPageMessage(GetType()));
                    Messenger.Default.Send(new PlayVideoMessage { VideoPath = default_video.VideoPath });
                    Messenger.Default.Send(new SurveyFinishedMessage
                    {
                        TalkToPatients = TalkToPatients,
                        WalkingDifficulty = WalkingDifficulty,
                        WhenDiscuss = WhenDiscuss,
                        HowManyPatients = HowManyPatients,
                        WhatPercentage = WhatPercentage,
                        User = _currentUser
                    });
                    ResetSelections();
                }, () => !string.IsNullOrEmpty(TalkToPatients) &&
                         !string.IsNullOrEmpty(WalkingDifficulty) &&
                         !string.IsNullOrEmpty(WhenDiscuss) &&
                         !string.IsNullOrEmpty(HowManyPatients) &&
                         !string.IsNullOrEmpty(WhatPercentage)));
            }
        }

        private void ResetSelections()
        {
            TalkToPatients = string.Empty;
            WalkingDifficulty = string.Empty;
            WhenDiscuss = string.Empty;
            HowManyPatients = string.Empty;
            WhatPercentage = string.Empty;
        }

        private string _talkToPatients;
        public string TalkToPatients
        {
            get { return _talkToPatients; }
            set
            {
                if (_talkToPatients == value) return;
                _talkToPatients = value;
                RaisePropertyChanged();
                NextPageCommand.RaiseCanExecuteChanged();
            }
        }

        private string _walkingDifficulty;
        public string WalkingDifficulty
        {
            get { return _walkingDifficulty; }
            set
            {
                if (_walkingDifficulty == value) return;
                _walkingDifficulty = value;
                RaisePropertyChanged();
                NextPageCommand.RaiseCanExecuteChanged();
            }
        }

        private string _whenDiscuss;
        public string WhenDiscuss
        {
            get { return _whenDiscuss; }
            set
            {
                if (_whenDiscuss == value) return;
                _whenDiscuss = value;
                RaisePropertyChanged();
                NextPageCommand.RaiseCanExecuteChanged();
            }
        }

        private string _howManyPatients;
        public string HowManyPatients
        {
            get { return _howManyPatients; }
            set
            {
                if (_howManyPatients == value) return;
                _howManyPatients = value;
                RaisePropertyChanged();
                NextPageCommand.RaiseCanExecuteChanged();
            }
        }

        private string _whatPercentage;
        public string WhatPercentage
        {
            get { return _whatPercentage; }
            set
            {
                if (_whatPercentage == value) return;
                _whatPercentage = value;
                RaisePropertyChanged();
                NextPageCommand.RaiseCanExecuteChanged();
            }
        }

        private Visitor _currentUser;
        private IDataService _dataService;
        public SurveyViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Messenger.Default.Register<NextPageMessage>(this, msg =>
            {
                if (msg.SourcePage != Pages.Reset) return;
                ResetSelections();
            });
            //Messenger.Default.Register<TypeSelectedMessage>(this, msg =>
            //{
            //    _currentUser = msg.User;
            //});
        }
    }
}