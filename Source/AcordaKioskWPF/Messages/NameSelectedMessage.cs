﻿namespace AcordaKioskWPF.Messages
{
    public class NameSelectedMessage : UserMessage
    {
        public string Name { get; set; }
    }

    public class NameSelectedPreMessage
    {
        
    }
}
