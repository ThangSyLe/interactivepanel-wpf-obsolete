﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcordaKioskWPF.Messages
{
    public class PushFooterMessage
    {
        public string PageName { get; set; }
    }
}
