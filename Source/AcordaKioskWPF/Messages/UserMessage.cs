﻿using AcordaKioskWPF.Model;

namespace AcordaKioskWPF.Messages
{
    public abstract class UserMessage
    {
        public Visitor User { get; set; }
    }
}
