﻿namespace AcordaKioskWPF.Messages
{
    public class SpecialtySelectedMessage : UserMessage
    {
        public string Specialty { get; set; }
    }
}
