﻿namespace AcordaKioskWPF.Messages
{
    public class VideoStateChangedMessage
    {
        public bool IsPlaying { get; set; }
        public string VideoName { get; set; }
    }
}
