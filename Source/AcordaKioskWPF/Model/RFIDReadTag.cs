namespace AcordaKioskWPF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RFIDReadTag
    {
        [Key]
        public int ReadID { get; set; }

        public string RFIDTagNum { get; set; }

        public int Antenna { get; set; }

        public double RSSI { get; set; }

        public DateTime LastRead { get; set; }

        public string ReaderMAC { get; set; }

        public string ReaderHost { get; set; }

        public string ReaderIPAddr { get; set; }

        public DateTime FirstRead { get; set; }

        public long TotalTimeSec { get; set; }
    }
}
