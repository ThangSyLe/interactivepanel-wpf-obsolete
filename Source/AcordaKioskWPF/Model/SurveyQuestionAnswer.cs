namespace AcordaKioskWPF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SurveyQuestionAnswer
    {
        [Key]
        public int SurveyID { get; set; }

        public string BadgeNum { get; set; }

        public int QuestionID { get; set; }

        public string Question { get; set; }

        public int AnswerID { get; set; }

        public string Answer { get; set; }
    }
}
