namespace AcordaKioskWPF.Model
{
    public class UserType
    {
        public const int PWP = 1;
        public const int CAREGIVER = 2;
        public const int HCP = 3;
        public const int OTHER = 4;

        public static int IdFromType(string type)
        {
            if (KioskSurveyQuestion.HcpType.AnswerMapping.ContainsKey(type))
                return KioskSurveyQuestion.HcpType.AnswerMapping[type];
            return -1;
        }
    }
}
