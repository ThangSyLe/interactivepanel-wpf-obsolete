﻿using System.Collections.Generic;

namespace AcordaKioskWPF.Model
{
    public class KioskSurveyQuestion
    {
        public static KioskSurveyQuestion QuestionFromTemplate(KioskSurveyQuestion template)
        {
            return new KioskSurveyQuestion
            {
                QuestionId = template.QuestionId,
                QuestionText = template.QuestionText,
                AnswerMapping = template.AnswerMapping,
                SelectedAnswers = new List<string>()
            };
        }

        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public Dictionary<string, int> AnswerMapping { get; set; }
        public List<string> SelectedAnswers { get; set; }

        public static KioskSurveyQuestion HcpType = new KioskSurveyQuestion
        {
            QuestionId = 1,
            QuestionText = "Which one best describes you?",
            AnswerMapping = new Dictionary<string, int> {
                {"A person with Parkinson's", 1},
                {"The caregiver/friend/family member of a person with Parkinson's", 2},
                {"A Healthcare Professional", 3},
                {"Researcher/Advocate/Interested Attendee", 4}
            }
        };

        public static KioskSurveyQuestion Specialty = new KioskSurveyQuestion
        {
            QuestionId = 2,
            QuestionText = "What is your specialty?",
            AnswerMapping = new Dictionary<string, int> {
                {"General Neurologist", 1},
                {"Movement Disorder Specialist", 2},
                {"Nurse", 3},
                {"Physical Therapist", 4},
                {"Physician Assistant", 5},
                {"Nurse Practitioner", 5},
                {"Primary Care Physician", 6},
                {"Other", 6}
            }
        };

        public static KioskSurveyQuestion Off = new KioskSurveyQuestion
        {
            QuestionId = 3,
            QuestionText = "Do you experience OFF periods?",
            AnswerMapping = new Dictionary<string, int> {
                {"Yes", 1},
                {"No", 2}
            }
        };

        public static KioskSurveyQuestion Symptom = new KioskSurveyQuestion
        {
            QuestionId = 4,
            QuestionText = "What are some of the physical symptoms you/your PD patients experience during/observe in your loved one during/report experiencing during/associate with an OFF period?",
            AnswerMapping = new Dictionary<string, int> {
                {"Pain", 1},
                {"Discomfort", 2},
                {"Slowness of Movement", 3},
                {"Difficulty Moving", 4},
                {"Inability to Move", 5},
                {"Tremors", 6},
                {"Fatigue", 7},
                {"Difficulty Speaking", 8},
                {"Difficulty Swallowing", 9},
                {"Rigidity", 10},
                {"None of these", 11},
                {"They don’t have OFF periods", 12},
                {"I don't know", 13}
            }
        };

        public static KioskSurveyQuestion Feeling = new KioskSurveyQuestion
        {
            QuestionId = 5,
            QuestionText = "What are the feelings you/your PD patients experience during/observe in your loved one during/report experiencing during/associate with an OFF period?",
            AnswerMapping = new Dictionary<string, int> {
                {"Anxious", 1},
                {"Sad", 2},
                {"Afraid", 3},
                {"Upset", 4},
                {"Weary", 5},
                {"Frustrated", 6},
                {"Helpless", 7},
                {"Vulnerable", 8},
                {"Embarrassed", 9},
                {"Concerned", 10},
                {"Out of Control", 11},
                {"Uncomfortable", 12},
                {"None of these", 13},
                {"They don’t report OFF periods", 14}
            }
        };

        public static KioskSurveyQuestion Word = new KioskSurveyQuestion
        {
            QuestionId = 6,
            QuestionText = "What one word best describes OFF periods to you?",
            AnswerMapping = new Dictionary<string, int> {
                {"Unexpected", 1},
                {"Controlling", 2},
                {"Frustrating", 3},
                {"Unpredictable", 4},
                {"Troubling", 5},
                {"Difficult", 6},
                {"Agonizing", 7},
                {"Annoying", 8},
                {"&*!%", 9},
                {"Heartbreaking", 10},
                {"Manageable", 11},
                {"None of these", 12}
            }
        };

        public static KioskSurveyQuestion LimitPWP = new KioskSurveyQuestion
        {
            QuestionId = 7,
            QuestionText = "How do OFF periods limit you?",
            AnswerMapping = new Dictionary<string, int> {
                {"Unable to do chores/housework", 1},
                {"Unable to take care of myself (get dressed, bathe, eat)", 2},
                {"Unable to socialize with friends outside of my home", 3},
                {"Unable to socialize with friends and family at home", 4},
                {"Unable to work", 5},
                {"I am able to do everything I need to do, but not things I want to do", 6},
                {"OFF periods do not have a significant impact on my life", 7}
            }
        };

        public static KioskSurveyQuestion LimitCarePartner = new KioskSurveyQuestion
        {
            QuestionId = 7,
            QuestionText = "How do OFF periods limit your loved one?",
            AnswerMapping = new Dictionary<string, int> {
                {"Unable to do chores/housework ", 1},
                {"Unable to take care of self (get dressed, bathe, eat)", 2},
                {"Unable to socialize with friends outside of the home", 3},
                {"Unable to socialize with friends and family at home", 4},
                {"Unable to work", 5},
                {"S/he is able to do everything s/he needs to do, but not things s/he wants to do", 6},
                {"OFF periods do not have a significant impact on his/her life", 7}
            }
        };

        public static KioskSurveyQuestion LimitHCP = new KioskSurveyQuestion
        {
            QuestionId = 7,
            QuestionText = "Thinking about your patient population, how do OFF periods limit them?",
            AnswerMapping = new Dictionary<string, int> {
                {"Unable to do chores/housework ", 1},
                {"Unable to take care of self (get dressed, bathe, eat)", 2},
                {"Unable to socialize with friends outside of the home", 3},
                {"Unable to socialize with friends and family at home", 4},
                {"Unable to work", 5},
                {"They are able to do everything they need to do, but not things they want to do", 6},
                {"OFF periods do not have a significant impact on their lives", 7}
            }
        };

        public static KioskSurveyQuestion LimitOther = new KioskSurveyQuestion
        {
            QuestionId = 7,
            QuestionText = "How do OFF periods limit people with Parkinson’s?",
            AnswerMapping = new Dictionary<string, int> {
                {"Unable to do chores/housework ", 1},
                {"Unable to take care of self (get dressed, bathe, eat)", 2},
                {"Unable to socialize with friends outside of the home", 3},
                {"Unable to socialize with friends and family at home", 4},
                {"Unable to work", 5},
                {"They are able to do everything they need to do, but not things they want to do", 6},
                {"OFF periods do not have a significant impact on their lives", 7}
            }
        };

        public static KioskSurveyQuestion TalkToPatients = new KioskSurveyQuestion
        {
            QuestionId = 2,
            QuestionText = "Do you usually talk about walking difficulties with your MS patients?",
            AnswerMapping = new Dictionary<string, int> {
                {"Yes", 1},
                {"No", 2}
            }
        };

        public static KioskSurveyQuestion WalkingDifficulty = new KioskSurveyQuestion
        {
            QuestionId = 3,
            QuestionText = "Who typically initiates a discussion about walking difficulty?",
            AnswerMapping = new Dictionary<string, int> {
                {"You", 1},
                {"The patient", 2},
                {"The care partner", 3},
                {"It is not discussed", 4}
            }
        };

        public static KioskSurveyQuestion WhenDiscuss = new KioskSurveyQuestion
        {
            QuestionId = 4,
            QuestionText = "At what point following a patient's initial MS diagnosis do you typically discuss walking difficulty?",
            AnswerMapping = new Dictionary<string, int> {
                {"It's already time—no additional trigger is needed", 1},
                {"When you observe signs of disease progression", 2},
                {"When the patient experiences an MS flare/exacerbation", 3},
                {"When the patient complains of walking difficulty", 4},
                {"When it becomes apparent that the patient requires an assistive device (eg, a cane)", 5},
                {"It is not discussed", 6}
            }
        };

        public static KioskSurveyQuestion HowManyPatients = new KioskSurveyQuestion
        {
            QuestionId = 5,
            QuestionText = "How many MS patients do you see per month?",
            AnswerMapping = new Dictionary<string, int> {
                {"More than 40", 1},
                {"26-40", 2},
                {"10-25", 3},
                {"Fewer than 10", 4},
                {"None", 5}
            }
        };

        public static KioskSurveyQuestion WhatPercentage = new KioskSurveyQuestion
        {
            QuestionId = 6,
            QuestionText = "What percentage of your patients have been prescribed AMPYRA (dalfampridine)?",
            AnswerMapping = new Dictionary<string, int> {
                {">50%", 1},
                {"31%-50%", 2},
                {"11%-30%", 3},
                {"0%-10%", 4}
            }
        };
    }
}
