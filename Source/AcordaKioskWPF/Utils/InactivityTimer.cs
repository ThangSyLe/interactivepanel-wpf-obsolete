﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace AcordaKioskWPF.Utils
{
	public class InactivityTimer
	{
		private DispatcherTimer _timer;

		private TimeSpan _timeout;
		public TimeSpan Timeout
		{
			get { return _timeout; }
			set
			{
			    if (value == _timeout) return;
			    var was_started = _timer != null && _timer.IsEnabled;
			    _timeout = value;
			    Reset(was_started);
			}
		}

		public event EventHandler UserInactive;
		public InactivityTimer(TimeSpan timeout, bool attachToMainWindow)
		{
			Timeout = timeout;
			Reset(false);

			if (attachToMainWindow)
			{
				AttachToUIElement(Application.Current.MainWindow);
			}
		}

		public void AttachToUIElement(UIElement elem)
		{
		    if (elem == null) return;
		    elem.AddHandler(Mouse.MouseDownEvent, new RoutedEventHandler(UIElement_InputDetected), true);
		    elem.AddHandler(Mouse.MouseMoveEvent, new RoutedEventHandler(UIElement_InputDetected), true);
		    elem.AddHandler(Keyboard.KeyDownEvent, new RoutedEventHandler(UIElement_InputDetected), true);
		    elem.AddHandler(Keyboard.KeyUpEvent, new RoutedEventHandler(UIElement_InputDetected), true);
		}

		public void Reset(bool start)
		{
			if (_timer == null)
			{
				_timer = new DispatcherTimer(DispatcherPriority.Background);
				_timer.Tick += OnTimerElapsed;
			}

			_timer.Interval = Timeout;

			if (start)
			{
				_timer.Start();
			}
		}

		public void Stop()
		{
			_timer.Stop();
		}

		public void Start()
		{
			Reset(true);
		}

		private void OnTimerElapsed(object sender, EventArgs e)
		{
			UserInactive?.Invoke(sender, e);
		}

		private void UIElement_InputDetected(object sender, RoutedEventArgs e)
		{
			// Don't want to reenable the timer if the user manually stopped it
			if (_timer.IsEnabled)
			{
				Reset(true);
			}
		}
	}
}
