﻿using System;
using System.Windows;
using System.Windows.Input;
using NLog;
using AcordaKioskWPF.Properties;

namespace AcordaKioskWPF.Utils
{
	public static class SwipeBehavior
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		#region Swipe Events

		public static readonly RoutedEvent SwipeLeftEvent = EventManager.RegisterRoutedEvent("SwipeLeft",
																				  RoutingStrategy.Bubble,
																				  typeof(RoutedEventHandler),
																				  typeof(SwipeBehavior));

		public static void AddSwipeLeftHandler(DependencyObject d, RoutedEventHandler handler)
		{
			var el = d as UIElement;
			if (el != null)
				el.AddHandler(SwipeLeftEvent, handler);
			else
			    Logger.Warn($"Adding a swipe handler to a dependency object that isn't a UIElement: {d}");
		}
		public static void RemoveSwipeLeftHandler(DependencyObject d, RoutedEventHandler handler)
		{
			var el = d as UIElement;
			if (el != null)
				el.RemoveHandler(SwipeLeftEvent, handler);
			else
			    Logger.Warn($"Removing a swipe handler from a dependency object that isn't a UIElement: {d}");
		}

		public static readonly RoutedEvent SwipeRightEvent = EventManager.RegisterRoutedEvent("SwipeRight",
																		  RoutingStrategy.Bubble,
																		  typeof(RoutedEventHandler),
																		  typeof(SwipeBehavior));

		public static void AddSwipeRightHandler(DependencyObject d, RoutedEventHandler handler)
		{
			var el = d as UIElement;
			if (el != null)
				el.AddHandler(SwipeRightEvent, handler);
			else
			    Logger.Warn($"Adding a swipe handler to a dependency object that isn't a UIElement: {d}");
		}
		public static void RemoveSwipeRightHandler(DependencyObject d, RoutedEventHandler handler)
		{
			var el = d as UIElement;
			if (el != null)
				el.RemoveHandler(SwipeRightEvent, handler);
			else
			    Logger.Warn($"Removing a swipe handler from a dependency object that isn't a UIElement: {d}");
		}

		public static readonly RoutedEvent SwipeUpEvent = EventManager.RegisterRoutedEvent("SwipeUp",
																  RoutingStrategy.Bubble,
																  typeof(RoutedEventHandler),
																  typeof(SwipeBehavior));

		public static void AddSwipeUpHandler(DependencyObject d, RoutedEventHandler handler)
		{
			var el = d as UIElement;
			if (el != null)
				el.AddHandler(SwipeUpEvent, handler);
			else
			    Logger.Warn($"Adding a swipe handler to a dependency object that isn't a UIElement: {d}");
		}
		public static void RemoveSwipeUpHandler(DependencyObject d, RoutedEventHandler handler)
		{
			var el = d as UIElement;
			if (el != null)
				el.RemoveHandler(SwipeUpEvent, handler);
			else
			    Logger.Warn($"Removing a swipe handler from a dependency object that isn't a UIElement: {d}");
		}

		public static readonly RoutedEvent SwipeDownEvent = EventManager.RegisterRoutedEvent("SwipeDown",
														  RoutingStrategy.Bubble,
														  typeof(RoutedEventHandler),
														  typeof(SwipeBehavior));

		public static void AddSwipeDownHandler(DependencyObject d, RoutedEventHandler handler)
		{
			var el = d as UIElement;
			if (el != null)
				el.AddHandler(SwipeDownEvent, handler);
			else
			    Logger.Warn($"Adding a swipe handler to a dependency object that isn't a UIElement: {d}");
		}
		public static void RemoveSwipeDownHandler(DependencyObject d, RoutedEventHandler handler)
		{
			var el = d as UIElement;
			if (el != null)
				el.RemoveHandler(SwipeDownEvent, handler);
			else
			    Logger.Warn($"Removing a swipe handler from a dependency object that isn't a UIElement: {d}");
		}

		#endregion

		#region Supports swipe property

		public static DependencyProperty SupportsSwipeProperty = DependencyProperty.RegisterAttached("SupportsSwipe", typeof(bool),
			typeof(SwipeBehavior), new FrameworkPropertyMetadata(false, OnSupportsSwipeChanged));

		public static bool GetSupportsSwipe(UIElement obj)
		{
			return (bool)obj.GetValue(SupportsSwipeProperty);
		}
		public static void SetSupportsSwipe(UIElement obj, bool value)
		{
			obj.SetValue(SupportsSwipeProperty, value);
		}

		#endregion

		#region Swipe Distance property

		public static DependencyProperty SwipeDistanceProperty = DependencyProperty.RegisterAttached("SwipeDistance", typeof(double),
			typeof(SwipeBehavior), new FrameworkPropertyMetadata(Settings.Default.SwipeDistance));

		public static double GetSwipeDistance(UIElement obj)
		{
			return (double)obj.GetValue(SwipeDistanceProperty);
		}
		public static void SetSwipeDistance(UIElement obj, bool value)
		{
			obj.SetValue(SwipeDistanceProperty, value);
		}

		#endregion

		public static void OnSupportsSwipeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			var el = obj as UIElement;
			if (el == null)
			{
			    Logger.Warn($"SupportsSwipe changed on an object that isn't a UIElement: {obj}. How did you manage to do this?");
			}
			else if ((bool)args.NewValue)
			{
				el.PreviewMouseDown += ElMouseDown;
			}
			else
			{
				el.PreviewMouseDown -= ElMouseDown;
			}
		}

		public static void ElMouseDown(object sender, MouseEventArgs e)
		{
			var el = sender as UIElement;
			if (el == null)
			{
			    Logger.Warn(
			        $"Handled a mouse down on an object that isn't a UIElement: {sender}. REALLY, how did you manage to do this?");
			}
			else
			{
				StartSwipe(el, e.GetPosition(el));
			}

			// Not sure if this is needed to stop behavior from eating events other controls may be interested in
			e.Handled = false;
		}

		public static void ElMouseUp(object sender, RoutedEventArgs e)
		{
			var el = sender as UIElement;
			if (el == null)
			{
			    Logger.Warn(
			        $"Handled a mouse up on an object that isn't a UIElement: {sender}. REALLY, how did you manage to do this?");
			}
			else
			{
				StopSwipe(el);
			}

			e.Handled = false;
		}

		public static void ElMouseMove(object sender, MouseEventArgs e)
		{
			var el = sender as UIElement;
			if (el == null)
			{
			    Logger.Warn(
			        $"Handled a mouse move on an object that isn't a UIElement: {sender}. REALLY, how did you manage to do this?");
			}
			else
			{
				var start_point = GetSwipeStartPoint(el);
				var cur_point = e.GetPosition(el);

				var diff = start_point - cur_point;
				var threshold = GetSwipeDistance(el);
				if (Math.Abs(diff.X) >= threshold)
				{
					el.RaiseEvent(diff.X > 0 ? new RoutedEventArgs(SwipeLeftEvent) : new RoutedEventArgs(SwipeRightEvent));
					StopSwipe(el);
				}
				else if (Math.Abs(diff.Y) >= threshold)
				{
					el.RaiseEvent(diff.X > 0 ? new RoutedEventArgs(SwipeDownEvent) : new RoutedEventArgs(SwipeUpEvent));
					StopSwipe(el);
				}
			}

			e.Handled = false;
		}

		private static void StartSwipe(UIElement el, Point pos)
		{
			el.PreviewMouseUp += ElMouseUp;
			el.PreviewMouseMove += ElMouseMove;
			SetSwipeStartPoint(el, pos);
		}

		private static void StopSwipe(UIElement el)
		{
			el.PreviewMouseUp -= ElMouseUp;
			el.PreviewMouseMove -= ElMouseMove;
		}

		#region Helper Property

		private static readonly DependencyProperty SwipeStartPointProperty = DependencyProperty.RegisterAttached("SwipeStartPoint", typeof (Point), typeof (SwipeBehavior));
		private static Point GetSwipeStartPoint(UIElement obj)
		{
			return (Point)obj.GetValue(SwipeStartPointProperty);
		}
		private static void SetSwipeStartPoint(UIElement obj, Point value)
		{
			obj.SetValue(SwipeStartPointProperty, value);
		}

		#endregion
	}
}
