﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace AcordaKioskWPF.Utils
{
    public static class ItemsControlExtensions
    {
        public static void AnimateScrollIntoView(this ItemsControl itemsControl, object item)
        {
            var scroll_viewer = VisualTreeHelpers.GetVisualChild<ScrollViewer>(itemsControl);
            if (scroll_viewer == null) return;

            var container = (UIElement) itemsControl.ItemContainerGenerator.ContainerFromItem(item);
            var index = itemsControl.ItemContainerGenerator.IndexFromContainer(container);
            var to_value = scroll_viewer.ScrollableWidth * ((double)index / (itemsControl.Items.Count - 1));

            var animation = new DoubleAnimation
            {
                From = scroll_viewer.HorizontalOffset,
                To = to_value,
                Duration = new Duration(TimeSpan.FromMilliseconds(250)),
                EasingFunction = new CubicEase
                {
                    EasingMode = EasingMode.EaseOut
                }
            };
            var sb = new Storyboard();
            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, scroll_viewer);
            Storyboard.SetTargetProperty(animation, new PropertyPath(AnimatedHorizScrollBehavior.HorizontalOffsetProperty));
            sb.Begin();
        }
    }
}
