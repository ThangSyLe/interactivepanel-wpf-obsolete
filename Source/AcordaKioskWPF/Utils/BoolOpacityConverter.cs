﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace AcordaKioskWPF.Utils
{
  [ValueConversion(typeof(bool), typeof(double))]
  public class BoolOpacityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object invert, CultureInfo culture)
    {
      var do_invert = (bool?) invert ?? false;
      return (bool)value && !do_invert ? 1.0 : 0.0;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return ((FrameworkElement)value).Opacity > 0.0;
    }
  }
}
