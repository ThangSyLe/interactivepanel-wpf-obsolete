﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using NLog;

namespace AcordaKioskWPF.Utils
{
	public static class TripleClickBehavior
	{
		private static Logger Logger = LogManager.GetCurrentClassLogger();

		#region tripleclick attached event

		public static readonly RoutedEvent TripleClickEvent = EventManager.RegisterRoutedEvent("TripleClick",
																						  RoutingStrategy.Bubble,
																						  typeof(RoutedEventHandler),
																						  typeof(TripleClickBehavior));

		public static void AddTripleClickHandler(DependencyObject d, RoutedEventHandler handler)
		{
			FrameworkElement el = d as FrameworkElement;
			if (el != null)
				el.AddHandler(TripleClickEvent, handler);
			else
				Logger.Warn("Adding a triple click handler to a dependency object that isn't a FrameworkElement: {0}", d);
		}
		public static void RemoveTripleClickHandler(DependencyObject d, RoutedEventHandler handler)
		{
			FrameworkElement el = d as FrameworkElement;
			if (el != null)
				el.RemoveHandler(TripleClickEvent, handler);
			else
				Logger.Warn("Removing a triple click handler to a dependency object that isn't a FrameworkElement: {0}", d);
		}

		#endregion

		#region Command

		public static readonly DependencyProperty TripleClickCommandProperty =
			DependencyProperty.RegisterAttached("TripleClickCommand", typeof(ICommand), typeof(TripleClickBehavior));

		public static ICommand GetTripleClickCommand(FrameworkElement obj)
		{
			return (ICommand)obj.GetValue(TripleClickCommandProperty);
		}
		public static void SetTripleClickCommand(FrameworkElement obj, ICommand value)
		{
			obj.SetValue(TripleClickCommandProperty, value);
		}

		public static readonly DependencyProperty TripleClickCommandParameterProperty =
			DependencyProperty.RegisterAttached("TripleClickCommandParameter", typeof(object), typeof(TripleClickBehavior));

		public static object GetTripleClickCommandParameter(FrameworkElement obj)
		{
			return obj.GetValue(TripleClickCommandParameterProperty);
		}
		public static void SetTripleClickCommandParameter(FrameworkElement obj, object value)
		{
			obj.SetValue(TripleClickCommandParameterProperty, value);
		}

		#endregion

		#region tripleclick behavior

		public static DependencyProperty SupportsTripleClickProperty =
		  DependencyProperty.RegisterAttached("SupportsTripleClick", typeof(bool), typeof(TripleClickBehavior), new UIPropertyMetadata(false, OnSupportsTripleClickChanged));
		public static bool GetSupportsTripleClick(FrameworkElement obj)
		{
			return (bool)obj.GetValue(SupportsTripleClickProperty);
		}
		public static void SetSupportsTripleClick(FrameworkElement obj, bool value)
		{
			obj.SetValue(SupportsTripleClickProperty, value);
		}

		public static void OnSupportsTripleClickChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			FrameworkElement el = obj as FrameworkElement;
			if (el == null)
				Logger.Warn("Triple click changed on an object that isn't a FrameworkElement: {0}.  How did you manage to do this?", el);
			else if ((bool)args.NewValue)
				el.PreviewMouseDown += ElMouseDown;
			else
				el.PreviewMouseDown -= ElMouseDown;
		}

		public static void ElMouseDown(object sender, RoutedEventArgs e)
		{
			FrameworkElement el = sender as FrameworkElement;
			if (el == null)
				Logger.Warn("Handled a mouse down an object that isn't a FrameworkElement: {0}.  REALLY, how did you manage to do this?", el);
			else
				SetNumClicks(el, GetNumClicks(el) + 1);
		}

		/// <summary>
		/// helper to track the number of clicks we've received on a given object
		/// </summary>
		public static readonly DependencyProperty NumClicksProperty = DependencyProperty.RegisterAttached("NumClicks", typeof(int), typeof(TripleClickBehavior), new PropertyMetadata(0, NumClicksChanged));
		public static int GetNumClicks(DependencyObject obj)
		{
			return (int)obj.GetValue(NumClicksProperty);
		}
		public static void SetNumClicks(DependencyObject obj, object value)
		{
			obj.SetValue(NumClicksProperty, value);
		}

		public static void NumClicksChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			//Logger.Debug("Num clicks changed for {0}. From {2} to {1}", d, e.NewValue, e.OldValue);

			int new_value = (int)e.NewValue;

			DispatcherTimer t = GetClicksTimer(d);
			if ((int)e.OldValue == 0)
			{
				//Logger.Debug("Kicking off new tripleclick timeout");
				t.Interval = TimeSpan.FromSeconds(1);
				t.Tick += TTick;
				t.Tag = d;
				t.Start();
			}
			else if (new_value == 0)
			{
				//Logger.Debug("Killing tripleclick timeout");
				t.Stop();
				t.Tick -= TTick;
			}

			if (new_value < 3) return;

			FrameworkElement el = d as FrameworkElement;
			if (el == null)
				Logger.Warn("Num clicks changed on an object that isn't a FrameworkElement: {0}. This is more believable, but also dumb", el);
			else
			{
				//Logger.Debug("Got tripleclick for element {0}. Raising tripleclick event.", d);
				try
				{
					el.RaiseEvent(new RoutedEventArgs(TripleClickEvent));

					ICommand cmd = GetTripleClickCommand(el);
					if (cmd != null)
					{
						cmd.Execute(GetTripleClickCommandParameter(el));
					}
				}
				catch (Exception ex)
				{
					Logger.ErrorException("Got exception raising tripleclick event", ex);
				}
				finally
				{
					SetNumClicks(el, 0);
				}
			}
		}

		static void TTick(object sender, EventArgs e)
		{
			//Logger.Debug("Got tripleclick timeout");
			DispatcherTimer t = sender as DispatcherTimer;
			FrameworkElement el = t.Tag as FrameworkElement;

			// Logger.Debug("Clearing out clicks for {0}", el);
			SetNumClicks(el, 0);
		}

		public static readonly DependencyProperty ClicksTimerProperty = DependencyProperty.RegisterAttached("ClicksTimer", typeof(DispatcherTimer), typeof(TripleClickBehavior),
																											new PropertyMetadata(new DispatcherTimer()));
		public static DispatcherTimer GetClicksTimer(DependencyObject obj)
		{
			return obj.GetValue(ClicksTimerProperty) as DispatcherTimer;
		}

		#endregion
	}
}
