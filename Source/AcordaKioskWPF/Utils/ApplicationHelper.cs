﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using AcordaKioskWPF.View;
using NLog;
using NLog.Config;

namespace AcordaKioskWPF.Utils
{
	public static class ApplicationExitCode
	{
		public const int NORMAL = 0;
		public const int MUTEX_FAILURE = 1;
		public const int NLOG_FAILURE = 2;
		public const int UNHANDLED_EXCEPTION = 3;
	}

	public static class ApplicationHelper
	{
		private static Logger _logger;

        public static bool InitLogging(string fileLogTarget = "std_file")
        {
            // register custom NLog target and create debug console
            ConfigurationItemFactory.Default.Targets.RegisterDefinition("Event", typeof(NLogEventTarget));

            // Need to init here so we'll catch any log messages ahead of time
            DebugTextControl.Init();

            if (LogManager.Configuration == null)
            {
                MessageBox.Show("NLog configuration not found in bin directory!", "Fatal Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            // verify that we have a std_file log so we don't miss logging important things
            if (!LogManager.Configuration.LoggingRules.SelectMany(r => r.Targets).Any(t => t.Name.Equals(fileLogTarget)))
            {
                MessageBox.Show("Log initialization failed. NLog.config must have a good file target!", "Fatal Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            _logger = LogManager.GetCurrentClassLogger();

            _logger.Info($"\r\n\r\n---------------------APP STARTUP at {DateTime.Now}----------------------");
            _logger.Info($"Starting application: v[{ Assembly.GetExecutingAssembly().GetName().Version }]");
            _logger.Debug("Debug logging enabled");
            
            return _logger != null;
        }

        public static Mutex CheckForMultipleInstances()
	    {
	        var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false);
	        var app_guid = ((GuidAttribute) attributes[0]).Value;

	        Debug.Assert(!string.IsNullOrEmpty(app_guid), "app guid should not be empty at startup");
	        var mutex = new Mutex(false, app_guid);
	        try
	        {
	            _logger.Debug($"Checking sharing mutex for app id: {app_guid}");
	            if (!mutex.WaitOne(0, false))
	            {
	                _logger.Warn("Another instance is already running.");
	                mutex = null;
	            }
	        }
	        catch (AbandonedMutexException ex)
	        {
	            _logger.Fatal(ex, "Previous application instance abandoned shared mutex");
	            mutex = null;
	        }

	        return mutex;
	    }
	}
}
