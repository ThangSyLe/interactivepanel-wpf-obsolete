﻿using System;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace AcordaKioskWPF.Utils
{
	// an NLog target that re-dispatches logging methods as static events
	// handlers can then write the log messages as they please
	[Target("Event")]
	public class NLogEventTarget : TargetWithLayout
	{
		public static event EventHandler<NLogEventTargetArgs> NLogEvent;

		protected override void Write(LogEventInfo logEvent)
		{
		    NLogEvent?.Invoke(this, new NLogEventTargetArgs(logEvent, Layout.Render(logEvent)));
		}

		static NLogEventTarget()
		{
			ConfigurationItemFactory.Default.Targets.RegisterDefinition("Event", typeof(NLogEventTarget));
		}
	}

	public class NLogEventTargetArgs : EventArgs
	{
		public LogEventInfo EventInfo;
		public string CompiledMessage;

		public NLogEventTargetArgs(LogEventInfo info, string msg)
		{
			EventInfo = info;
			CompiledMessage = msg;
		}
	}
}
